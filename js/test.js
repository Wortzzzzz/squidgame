class Doll2{
    constructor() {
        const loader = new THREE.GLTFLoader();
        loader.load("../modells/scene.gltf", (gltf) => {
            scene.add( gltf.scene);
            
            gltf.scene.scale.set(.3, .3, .3);
            gltf.scene.position.set(0, -1, 0);
            this.doll = gltf.scene;
            gltf.scene.position.z = 1;
            gltf.scene.position.x = start_position;
            scene.add(  gltf.scene );
            this.player =  gltf.scene;
            this.playerInfo = {
                positionX: start_position,
                velocity: 0
            }
            }
        
    )
    }

    run(){
        this.playerInfo.velocity = .03
     }
    
     stop(){
        // this.playerInfo.velocity = 0
         gsap.to(this.playerInfo, {velocity: 0, duration: .1})
     }
    
     check(){
        if(this.playerInfo.velocity > 0 && !isLookingBackward){
        
            text.innerText = "You lose!"
            gameStat = "over";
        }
        if(this.playerInfo.positionX < end_position + .4){
            
            text.innerText = "You win!"
            gameStat = "over";
        }
    }
     update(){
       
        this.check()   
       
        this.playerInfo.positionX -= this.playerInfo.velocity   
         this.player.position.x = this.playerInfo.positionX
     }   

}
